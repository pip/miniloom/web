//export function bindIcons (icons) {
//  let computed = {}
//  for (const [key, value] of Object.entries(icons)) {
//    key = key[0].toLowerCase() + key.slice(1)
//    computed[key] = () => value
//  }
//  return computed
//}

export function grabValues(eventOrForm, values = null) {
  let form = eventOrForm
  if (eventOrForm.target) form = eventOrForm.target

  const result = []
    .concat(...form)
    .reduce(
      (obj, el) =>
        (values ? values.indexOf(el.name) !== -1 : true) && el.name
          ? { ...obj, [el.name]: el.value }
          : obj,
      {},
    )

  return result
}
