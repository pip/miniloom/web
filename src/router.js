import Vue from 'vue'
import Router from 'vue-router'

import store from '@/store'

import Home from './views/Home.vue'
import CrearCuenta from './views/CrearCuenta.vue'
import CodigoDeConducta from './views/CodigoDeConducta.vue'
import IniciarSesion from './views/IniciarSesion.vue'
import VincularTelegram from './views/VincularTelegram.vue'
import CerrarSesion from './views/CerrarSesion.vue'
import Barcas from './views/Barcas.vue'
import Barca from './views/Barca.vue'
import Faq from './views/Faq.vue'
import Licencia from './views/Licencia.vue'
import CrearBarca from './views/CrearBarca.vue'
import Consenso from './views/Consenso.vue'
import CrearConsenso from './views/CrearConsenso.vue'
import EliminarPropuesta from './views/EliminarPropuesta.vue'
import AbordarBarca from './views/AbordarBarca.vue'
import AbandonarBarca from './views/AbandonarBarca.vue'
import NotFound from './views/NotFound.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { fullScreen: true },
    },
    {
      path: '/crearCuenta',
      name: 'crear cuenta',
      component: CrearCuenta,
      meta: { fullScreen: true },
    },
    {
      path: '/iniciarSesion',
      name: 'iniciar sesion',
      component: IniciarSesion,
      meta: { fullScreen: true },
    },
    {
      path: '/vincularATelegram',
      name: 'vincular a telegram',
      component: VincularTelegram,
      meta: { requiresAuth: true },
    },
    {
      path: '/cerrarSesion',
      name: 'cerrar sesion',
      component: CerrarSesion,
    },
    {
      path: '/codigoDeConducta',
      name: 'codigo de conducta',
      component: CodigoDeConducta,
    },
    {
      path: '/faq',
      name: 'faq',
      component: Faq,
    },
    {
      path: '/licencia',
      name: 'licencia',
      component: Licencia,
    },
    {
      path: '/barcas',
      name: 'barcas',
      component: Barcas,
      meta: { requiresAuth: true },
    },
    {
      path: '/barcas/:id(\\d+)',
      name: 'barca',
      component: Barca,
      meta: {
        requiresAuth: true,
        backCallback: () => history.back(),
      },
    },
    {
      path: '/barcas/crear',
      name: 'crear barca',
      component: CrearBarca,
      meta: {
        requiresAuth: true,
        backCallback: (router, route) => router.push(`/barcas`),
      },
    },
    //{
    //  path: '/consensos/archivados',
    //  name: 'consensos archivados',
    //  component: NotFound,
    //  meta: { requiresAuth: true },
    //},
    {
      path: '/barcas/:barcaId/consensos/:id(\\d+)',
      name: 'consenso',
      component: Consenso,
      meta: {
        requiresAuth: true,
        backCallback: () => history.back(),
      },
    },
    {
      path: '/barcas/:barcaId/consensos/:id(\\d+)/eliminar',
      name: 'eliminar consenso',
      component: EliminarPropuesta,
      meta: {
        requiresAuth: true,
        backCallback: (router, route) =>
          router.push(
            `/barcas/${route.params.barcaId}/consensos/${route.params.id}`,
          ),
      },
    },
    {
      path: '/barcas/:id(\\d+)/consensos/crear',
      name: 'crear consenso',
      component: CrearConsenso,
      meta: {
        requiresAuth: true,
        backCallback: (router, route) =>
          router.push(`/barcas/${route.params.id}`),
      },
    },
    {
      path: '/barcas/:id(\\d+)/abordar',
      name: 'abordar barca',
      component: AbordarBarca,
      meta: {
        requiresAuth: true,
        backCallback: () => history.back(),
      },
    },
    {
      path: '/barcas/:id(\\d+)/abandonar',
      name: 'abandonar barca',
      component: AbandonarBarca,
      meta: {
        requiresAuth: true,
        backCallback: (router, route) =>
          router.push(`/barcas/${route.params.id}`),
      },
    },
    {
      path: '*',
      name: 'not found',
      component: NotFound,
      meta: {
        backCallback: () => history.back(),
      },
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.loggedIn && store.state.initialLoadDone) {
      next({
        path: '/iniciarSesion',
        query: { redirect: to.fullPath },
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
