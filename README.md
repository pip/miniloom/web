# lumi-frontend

## instalar dependencias

necesitas node.js y npm (viene con node.js generalmente)

```
npm i
```

## configurar entorno

```
cp .env.example .env
# editar .env con la url de la API, por defecto está en producción
```

## modo de desarrollo

```
npm run serve
# anda a localhost:8080
```

## hacer que quede bien el código (se hace solo al commitear!)

```
npm run prettify
```

## compilar para producción

```
npm build
# el resultado esta en dist/
```
